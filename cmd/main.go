package main

import (
	"bufio"
	"go-cli-billing/internal/handler"
	"go-cli-billing/internal/model/entity"
	"go-cli-billing/internal/repository"
	"go-cli-billing/internal/service"
	"os"
)

func main() {
	var bill entity.Bill
	repo := repository.NewBillRepository(&bill)
	serv := service.NewBillService(repo)
	// hand := handler.NewBillHandler(serv)
	// _ = hand

	// hand.AddItem("Cake", 100, 2)
	// hand.AddItem("Bread", 200, 3)
	// hand.AddItem("Cookie", 120, 5)
	// hand.AddItem("Tea", 70, 1)
	// hand.AddItem("Chocolate", 150, 3)
	// hand.AddItem("Coke", 140, 1)
	// hand.AddItem("Peanut", 20, 10)
	// hand.ShowBill()
	// hand.UpdateItem("Coffee", 150, 2, -1)
	// hand.UpdateItem("Coffee", 150, 1, 4)
	// hand.ShowBill()
	// hand.DeleteItem(1)
	// hand.ShowBill()

	promptHandler := handler.NewPromptHandler(serv)
	promptHandler.PromptOptions(bufio.NewReader(os.Stdin))
}
