package service

import (
	"fmt"
	"go-cli-billing/internal/model/entity"
	"go-cli-billing/internal/model/web"
	"go-cli-billing/internal/repository"
	"go-cli-billing/internal/util"
)

type BillService interface {
	AddItem(request web.ItemRequest)
	ShowBill() web.BillResponse
	UpdateItem(request web.ItemRequest, id int)
	DeleteItem(id int)
}

type billServiceImpl struct {
	billRepository repository.BillRepository
}

func NewBillService(billRepository repository.BillRepository) BillService {
	return &billServiceImpl{
		billRepository: billRepository,
	}
}

func (b *billServiceImpl) AddItem(request web.ItemRequest) {
	item := &entity.Item{
		ItemName: request.ItemName,
		Price:    request.Price,
		Quantity: request.Quantity,
	}
	b.billRepository.AddItem(item)
}

func (b *billServiceImpl) ShowBill() web.BillResponse {
	bill := b.billRepository.ShowBill()

	response := util.ToBillResponse(bill)

	return response
}

func (b *billServiceImpl) UpdateItem(request web.ItemRequest, id int) {
	item := &entity.Item{
		ItemId:   id,
		ItemName: request.ItemName,
		Price:    request.Price,
		Quantity: request.Quantity,
	}
	_, err := b.billRepository.UpdateItem(item)
	if err != nil {
		fmt.Println(err)
	}
}

func (b *billServiceImpl) DeleteItem(id int) {
	b.billRepository.DeleteItem(id)
}
