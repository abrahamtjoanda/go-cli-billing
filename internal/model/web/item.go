package web

type ItemRequest struct {
	ItemName string
	Price    float64
	Quantity int
}

type ItemResponse struct {
	ItemId   int
	ItemName string
	Price    float64
	Quantity int
}
