package web

type BillResponse struct {
	BillId string
	Items  []ItemResponse
	Tip    float64
}
