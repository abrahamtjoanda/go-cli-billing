package entity

type Bill struct {
	BillId string
	Items  map[int]Item
	Tip    float64
}
