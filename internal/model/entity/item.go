package entity

type Item struct {
	ItemId   int
	ItemName string
	Price    float64
	Quantity int
}
