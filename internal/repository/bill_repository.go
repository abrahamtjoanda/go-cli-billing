package repository

import (
	"fmt"
	"go-cli-billing/internal/model/entity"
	"go-cli-billing/internal/util"
)

type BillRepository interface {
	AddItem(item *entity.Item) *entity.Item
	ShowBill() *entity.Bill
	UpdateItem(item *entity.Item) (*entity.Item, error)
	DeleteItem(id int) error
	// SaveBill(bill *entity.Bill)
	// PrintBill(bill *entity.Bill)
}

type billRepositoryImpl struct {
	Bill   *entity.Bill
	ItemId util.AutoInc
}

func NewBillRepository(bill *entity.Bill) BillRepository {
	bill.Items = make(map[int]entity.Item)
	return &billRepositoryImpl{
		Bill: bill,
	}
}

func (b *billRepositoryImpl) AddItem(item *entity.Item) *entity.Item {
	itemId := b.ItemId.ID()
	item.ItemId = itemId

	b.Bill.Items[itemId] = *item

	return item
}

func (b *billRepositoryImpl) ShowBill() *entity.Bill {
	return b.Bill
}

func (b *billRepositoryImpl) UpdateItem(item *entity.Item) (*entity.Item, error) {
	_, ok := b.Bill.Items[item.ItemId]
	if ok {
		b.Bill.Items[item.ItemId] = *item
	} else {
		msg := fmt.Sprintf(`Failed to update, item %s with id %d not found`, item.ItemName, item.ItemId)

		return item, fmt.Errorf(msg)
	}

	return item, nil
}

func (b *billRepositoryImpl) DeleteItem(id int) error {
	_, ok := b.Bill.Items[id]
	if ok {
		delete(b.Bill.Items, id)
	} else {
		msg := fmt.Sprintf(`Failed to delete, item with id %d not found`, id)

		return fmt.Errorf(msg)
	}

	return nil
}
