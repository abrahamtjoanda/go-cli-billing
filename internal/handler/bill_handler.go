package handler

import (
	"fmt"
	"go-cli-billing/internal/service"
	"go-cli-billing/internal/util"
)

type BillHandler interface {
	AddItem(itemName string, price float64, quantity int)
	ShowBill()
	UpdateItem(itemName string, price float64, quantity int, id int)
	DeleteItem(id int)
}

type billHandlerImpl struct {
	BillService service.BillService
}

func NewBillHandler(billService service.BillService) BillHandler {
	return billHandlerImpl{
		BillService: billService,
	}
}

func (b billHandlerImpl) AddItem(itemName string, price float64, quantity int) {
	item := util.ToItemRequest(itemName, price, quantity)

	b.BillService.AddItem(item)
}

func (b billHandlerImpl) ShowBill() {
	response := b.BillService.ShowBill()

	fmt.Println(response)
}

func (b billHandlerImpl) UpdateItem(itemName string, price float64, quantity int, id int) {
	item := util.ToItemRequest(itemName, price, quantity)

	b.BillService.UpdateItem(item, id)
}

func (b billHandlerImpl) DeleteItem(id int) {
	b.BillService.DeleteItem(id)
}
