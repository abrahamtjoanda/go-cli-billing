package handler

import (
	"bufio"
	"fmt"
	"go-cli-billing/internal/service"
	"go-cli-billing/internal/util"
	"strconv"
	"strings"
)

type PromptHandler interface {
	PromptOptions(reader *bufio.Reader)
	addItem(reader *bufio.Reader)
	showBill(reader *bufio.Reader)
	updateItem(reader *bufio.Reader)
	deleteItem(reader *bufio.Reader)
}

type promptHandlerImpl struct {
	BillService service.BillService
}

func NewPromptHandler(billService service.BillService) PromptHandler {
	return &promptHandlerImpl{
		BillService: billService,
	}
}

func (b *promptHandlerImpl) PromptOptions(reader *bufio.Reader) {
	var opt string
	opt, _ = util.ReadInput("Choose option (1 - add item, 2 - show bill, 3 - update item, 4 - delete item, 5 - exit): ", reader)
	switch opt {
	case "1":
		b.addItem(reader)
	case "2":
		b.showBill(reader)
	case "3":
		b.updateItem(reader)
	case "4":
		b.deleteItem(reader)
	case "5":
		return
	case "cancel":
		b.PromptOptions(reader)
	default:
		fmt.Println("Invalid option...")
		b.PromptOptions(reader)
	}
}

func (b *promptHandlerImpl) addItem(reader *bufio.Reader) {
	itemName, _ := util.ReadInput("Add an item: ", reader)
	if itemName == "" {
		fmt.Println("Item name cannot be empty")
		b.PromptOptions(reader)
		return
	}

	itemPrice, _ := util.ReadInput("Price: ", reader)
	price, err := strconv.ParseFloat(itemPrice, 64)
	if err != nil {
		fmt.Println("The price must be a number")
		b.PromptOptions(reader)
		return
	}

	itemQuantity, _ := util.ReadInput("Quantity: ", reader)
	quantity, err := strconv.Atoi(itemQuantity)
	if err != nil {
		fmt.Println("The quantity must be a number")
		b.PromptOptions(reader)
		return
	}

	item := util.ToItemRequest(itemName, price, quantity)
	b.BillService.AddItem(item)
	b.PromptOptions(reader)
}

func (b *promptHandlerImpl) showBill(reader *bufio.Reader) {
	var total float64 = 0
	response := b.BillService.ShowBill()

	result := "Bill breakdown: \n"
	result += fmt.Sprintf(strings.Repeat("=", 43) + "\n")
	result += fmt.Sprintf("%v %20v %5v %10v\n", "Name", "Price", "Qty", "Amount")
	result += fmt.Sprintf(strings.Repeat("=", 43) + "\n")

	for _, item := range response.Items {
		totalItemPrice := item.Price * float64(item.Quantity)
		total += totalItemPrice
		result += fmt.Sprintf("%v\n%25v %5v %10v\n", item.ItemName, item.Price, item.Quantity, totalItemPrice)

	}
	result += fmt.Sprintf(strings.Repeat("=", 43) + "\n")
	result += fmt.Sprintf("%v %38v\n", "Tip", response.Tip)
	result += fmt.Sprintf("%v %36v\n", "Total", total+response.Tip)
	result += fmt.Sprintf(strings.Repeat("=", 43) + "\n")

	fmt.Println(result)

	b.PromptOptions(reader)
}

func (b *promptHandlerImpl) updateItem(reader *bufio.Reader) {
	response := b.BillService.ShowBill()
	items := response.Items

	fmt.Println("Displaying items:")
	util.PrintItems(items)

	itemId, err := util.ReadItemId(reader, items, "Pick which item to update (select item id): ")
	if err != nil {
		b.PromptOptions(reader)
		return
	}

	itemName, err := util.ReadItemField(reader, "Change item name (leave blank if no change): ", items[itemId-1].ItemName)
	if err != nil {
		b.PromptOptions(reader)
		return
	}

	itemPrice, err := util.ReadItemPrice(reader, "Change price (leave blank if no change): ", strconv.FormatFloat(items[itemId-1].Price, 'f', 2, 64))
	if err != nil {
		b.PromptOptions(reader)
		return
	}

	itemQuantity, err := util.ReadItemQuantity(reader, "Change quantity (leave blank if no change): ", strconv.Itoa(items[itemId-1].Quantity))
	if err != nil {
		b.PromptOptions(reader)
		return
	}

	item := util.ToItemRequest(itemName, itemPrice, itemQuantity)
	b.BillService.UpdateItem(item, itemId)
	b.PromptOptions(reader)
}

func (b *promptHandlerImpl) deleteItem(reader *bufio.Reader) {
	response := b.BillService.ShowBill()
	items := response.Items

	fmt.Println("Displaying items:")
	util.PrintItems(items)

	itemId, err := util.ReadItemId(reader, items, "Pick which item to delete (select item id): ")
	if err != nil {
		b.PromptOptions(reader)
		return
	}

	b.BillService.DeleteItem(itemId)
	fmt.Println("Item deleted")
	b.PromptOptions(reader)
}
