package util

import (
	"bufio"
	"fmt"
	"go-cli-billing/internal/model/web"
	"strconv"
	"strings"
)

func PrintItems(items []web.ItemResponse) {
	fmt.Printf("%-5v %-20v %-10v %v\n", "Id", "Item", "Qty", "Price")
	fmt.Println(strings.Repeat("=", 50))
	for _, item := range items {
		fmt.Printf("%-5v %-20v %-10v %.2f\n", item.ItemId, item.ItemName, item.Quantity, item.Price)
	}
}

func ReadItemId(reader *bufio.Reader, items []web.ItemResponse, prompt string) (int, error) {
	itemId, _ := ReadInput(prompt, reader)
	id, err := strconv.Atoi(itemId)
	if err != nil {
		fmt.Println("The ID must be a number")
		return 0, err
	} else if id < 1 || id > len(items) {
		fmt.Println("The ID must be between 1 and", len(items))
		return 0, err
	}
	return id, nil
}

func ReadItemField(reader *bufio.Reader, prompt, defaultValue string) (string, error) {
	itemField, _ := ReadInput(prompt, reader)
	if itemField == "" {
		return defaultValue, nil
	}
	return itemField, nil
}

func ReadItemPrice(reader *bufio.Reader, prompt, defaultValue string) (float64, error) {
	itemPrice, err := ReadItemField(reader, prompt, defaultValue)
	if err != nil {
		return 0, err
	}

	price, err := strconv.ParseFloat(itemPrice, 64)
	if err != nil {
		fmt.Println("The price must be a number")
		return 0, err
	}
	return price, nil
}

func ReadItemQuantity(reader *bufio.Reader, prompt, defaultValue string) (int, error) {
	itemQuantity, err := ReadItemField(reader, prompt, defaultValue)
	if err != nil {
		return 0, err
	}

	quantity, err := strconv.Atoi(itemQuantity)
	if err != nil {
		fmt.Println("The quantity must be a number")
		return 0, err
	}
	return quantity, nil
}
