package util

import (
	"bufio"
	"fmt"
	"strings"
)

func ReadInput(prompt string, r *bufio.Reader) (string, error) {
	fmt.Print(prompt)
	input, err := r.ReadString('\n')
	input = strings.TrimSpace(input)

	return input, err
}

// func ScanInput(prompt string) (string, error) {
// 	var input string
// 	fmt.Print(prompt)
// 	_, err := fmt.Scanln(&input)

// 	return input, err
// }
