package util

import (
	"go-cli-billing/internal/model/entity"
	"go-cli-billing/internal/model/web"
	"sort"
)

func ToBillResponse(bill *entity.Bill) web.BillResponse {
	items := bill.Items
	itemResponse := []web.ItemResponse{}

	for _, v := range items {
		itemResponse = append(itemResponse, ToItemResponse(&v))
	}

	sort.Slice(itemResponse, func(i, j int) bool {
		return itemResponse[i].ItemId < itemResponse[j].ItemId
	})

	return web.BillResponse{
		BillId: bill.BillId,
		Items:  itemResponse,
		Tip:    bill.Tip,
	}
}

func ToItemRequest(itemName string, price float64, quantity int) web.ItemRequest {
	return web.ItemRequest{
		ItemName: itemName,
		Price:    price,
		Quantity: quantity,
	}
}

func ToItemResponse(item *entity.Item) web.ItemResponse {
	return web.ItemResponse{
		ItemId:   item.ItemId,
		ItemName: item.ItemName,
		Price:    item.Price,
		Quantity: item.Quantity,
	}
}
