package util

import "sync"

type AutoInc struct {
	sync.Mutex // ensures autoInc is goroutine-safe
	id         int
}

func (a *AutoInc) ID() (id int) {
	a.Lock()
	defer a.Unlock()

	a.id++
	return a.id
}
