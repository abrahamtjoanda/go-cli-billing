package test

import (
	"go-cli-billing/internal/model/entity"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAddItem(t *testing.T) {
	repository := setupBillRepository()
	result := repository.AddItem(&entity.Item{ItemName: "Cake", Price: 100, Quantity: 2})

	expected := &entity.Item{
		ItemId:   1,
		ItemName: "Cake",
		Price:    100,
		Quantity: 2,
	}

	assert.Equal(t, expected, result)
}

func TestShowBill(t *testing.T) {
	repository := setupBillRepository()
	repository.AddItem(&entity.Item{ItemName: "Cake", Price: 100, Quantity: 2})
	repository.AddItem(&entity.Item{ItemName: "Bread", Price: 200, Quantity: 3})
	result := repository.ShowBill()

	expected := &entity.Bill{
		Items: map[int]entity.Item{
			1: {
				ItemId:   1,
				ItemName: "Cake",
				Price:    100,
				Quantity: 2,
			},
			2: {
				ItemId:   2,
				ItemName: "Bread",
				Price:    200,
				Quantity: 3,
			},
		},
	}

	assert.Equal(t, expected, result)
}

func TestUpdateItem(t *testing.T) {
	repository := setupBillRepository()
	item := repository.AddItem(&entity.Item{ItemName: "Cake", Price: 100, Quantity: 2})

	result, err := repository.UpdateItem(&entity.Item{ItemId: item.ItemId, ItemName: "Bread", Price: 200, Quantity: 3})

	expected := &entity.Item{
		ItemId:   1,
		ItemName: "Bread",
		Price:    200,
		Quantity: 3,
	}

	assert.Nil(t, err)
	assert.Equal(t, expected, result)
}

func TestDeleteItem(t *testing.T) {
	repository := setupBillRepository()
	repository.AddItem(&entity.Item{ItemName: "Cake", Price: 100, Quantity: 2})
	repository.AddItem(&entity.Item{ItemName: "Bread", Price: 200, Quantity: 3})

	err := repository.DeleteItem(1)

	result := repository.ShowBill()

	expected := &entity.Bill{
		Items: map[int]entity.Item{
			2: {
				ItemId:   2,
				ItemName: "Bread",
				Price:    200,
				Quantity: 3,
			},
		},
	}

	assert.Nil(t, err)
	assert.Equal(t, expected, result)
}
