package test

import (
	"go-cli-billing/internal/handler"
	"go-cli-billing/internal/model/entity"
	"go-cli-billing/internal/repository"
	"go-cli-billing/internal/service"
)

func setupBillRepository() repository.BillRepository {
	var bill entity.Bill
	repository.NewBillRepository(&bill)
	return repository.NewBillRepository(&bill)
}

func setupPromptHandler() handler.PromptHandler {
	var bill entity.Bill
	repo := repository.NewBillRepository(&bill)
	serv := service.NewBillService(repo)
	return handler.NewPromptHandler(serv)
}
