package test

import (
	"bufio"
	"os"
	"testing"
)

func TestPromptOptions(t *testing.T) {
	handler := setupPromptHandler()

	in, _ := os.Open("in.txt")
	os.Stdin = in
	reader := bufio.NewReader(os.Stdin)

	handler.PromptOptions(reader)
}
