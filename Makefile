# make run
run:
	clear
	go run cmd/main.go

# make testbill
testbill:
	clear
	go test -v ./test

# make testprompt
testprompt:
	clear
	go test -v ./test -run=TestPromptOptions

# make build
build:
	clear
	go build -o bin/main ./cmd/main.go

# make buildtest
buildtest:
	clear
	go test -c -o bin/test ./test